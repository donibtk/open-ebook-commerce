class CreateEbookOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :ebook_orders do |t|
      t.decimal :value
      t.belongs_to :order, foreign_key: true
      t.belongs_to :ebook, foreign_key: true
      t.timestamps
    end
  end
end
