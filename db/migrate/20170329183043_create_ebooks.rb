class CreateEbooks < ActiveRecord::Migration[5.0]
  def change
    create_table :ebooks do |t|
      t.string :title
      t.string :author
      t.integer :words
      t.decimal :price
      t.string :summary
      t.integer :year
      t.boolean :available

      t.timestamps
    end
  end
end
