class AddNameToManager < ActiveRecord::Migration[5.0]
  def change
    add_column :managers, :first_name, :string
    add_column :managers, :surname, :string
  end
end
