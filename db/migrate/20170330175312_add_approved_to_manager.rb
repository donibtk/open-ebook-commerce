class AddApprovedToManager < ActiveRecord::Migration[5.0]
  def change
    add_column :managers, :approved, :boolean, :default => false, :null => false
    add_index  :managers, :approved
  end
end
