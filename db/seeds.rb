# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
include ActionDispatch::TestProcess

Manager.new(:email => 'admin@admin.edu', :password => 'abc123',
  :first_name => "Admin", :surname => "Default",
  :password_confirmation => 'abc123', :approved => true).save! if Manager.all.count == 0

Ebook.populate(10) do |e|
  e.title = Faker::Book.title
  e.author = Faker::Book.author
  e.words = 10..10000
  e.price = [1.00, 2.00, 5.00, 10.00, 15.00, 20.00, 30.00, 50.00, 100.00]

  e.summary = Populator.sentences(5)
  e.year = 1800..2017
  e.available = [true, true, true, false]
end

Ebook.all.each do |e|
  if e.gender_list.blank?
    e.gender_list = "#{Faker::Book.genre},#{Faker::Book.genre}"
    e.cover = fixture_file_upload(Dir.glob(File.join(Rails.root, 'test/files', '*.jpg')).sample, 'image/jpeg')
    e.epub = fixture_file_upload(Rails.root.join('test', 'files', 'ebook.epub'), 'application/epub+zip')
    e.pdf = fixture_file_upload(Rails.root.join('test', 'files', 'ebook.pdf'), 'application/pdf')
    e.save
  end
end
