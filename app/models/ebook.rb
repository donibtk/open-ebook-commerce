class Ebook < ApplicationRecord
  has_many :orders, through: :ebook_orders
  has_many :ebook_orders
  has_attached_file :cover
  validates_attachment :cover,
      content_type: {content_type: "image/jpeg"},
      size: {in: 10..200.kilobytes }
  has_attached_file :pdf,
                    :url => '/:class/:id/:style.:extension',
                    :path => ':rails_root/assets/:class/:id_partition/:style.:extension'
  validates_attachment :pdf,
      content_type: {content_type: "application/pdf"}
  has_attached_file :epub,
                    :url => '/:class/:id/:style.:extension',
                    :path => ':rails_root/assets/:class/:id_partition/:style.:extension'
  validates_attachment :epub,
      content_type: {content_type: "application/epub+zip"}

    acts_as_taggable_on :genders
    #attr_accessible :gender_list
    def self.search(term, current_page, category)
      if term
        #term = ActiveRecord::Base.sanitize(term)
        if category && category != 'todos'
          page(current_page).where("available = ? AND (title LIKE ? OR summary LIKE ? OR author LIKE ?)", true, "%#{term}%", "%#{term}%", "%#{term}%").order('id DESC').tagged_with(category).per(10)
        else
          page(current_page).where("available = ? AND (title LIKE ? OR summary LIKE ? OR author LIKE ?)", true, "%#{term}%", "%#{term}%", "%#{term}%").order('id DESC').per(10)
        end
      elsif category && category != 'todos'
        page(current_page).where('available = ?', true).order('id DESC').tagged_with(category).per(10)
      else
        page(current_page).where('available = ?', true).order('id DESC').per(10)
      end
    end

    def delete_files
      File.delete(cover.path) unless cover.path.nil?
      File.delete(pdf.path) unless pdf.path.nil?
      File.delete(epub.path) unless epub.path.nil?
    end

    before_destroy {|e|
      # ?? só funciona assim, se colocar e.delete_files não funciona
      Ebook.find(e.id).delete_files
    }

end
