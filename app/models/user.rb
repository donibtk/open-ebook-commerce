class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :orders, -> {where(paid: true)}
  has_many :ebook_orders, through: :orders
  has_many :ebooks, through: :ebook_orders

  def has_ebook?(ebook_id)
    !!ebooks.find_by_id(ebook_id)
  end

  def name
    begin
      first_name + " " + surname
    rescue
      nil
    end
  end
  
end
