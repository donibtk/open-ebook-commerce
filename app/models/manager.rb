class Manager < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validate :check_if_can_disable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  def active_for_authentication?
    super && approved?
  end

  def inactive_message
    if !approved?
      :not_approved
    else
      super # Use whatever other message
    end
  end

  def check_if_can_disable
    return if new_record?
    if approved == false && Manager.where('approved' => true).count == 1
      errors.add(:manager, 'Não é possivel desativar todos os gerentes')
    end
  end
end
