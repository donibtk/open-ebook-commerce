class Order < ApplicationRecord
  before_update :check_payment
  has_many :ebooks, through: :ebook_orders
  has_many :ebook_orders
  belongs_to :user

  private
    def check_payment
      logger.info "mudando " + self.paid_was.to_s
      logger.info "para " + self.paid.to_s
      if self.paid != self.paid_was
        ActionCable.server.broadcast "orders_#{self.user_id}",
            id: self.id.to_s,
            content: OrderController.new.render_to_string(self)
        UserMailer.ebooks_download(self).deliver_now if self.paid
      end
      return true
    end
end
