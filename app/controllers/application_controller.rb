class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :store_current_location, :unless => :devise_controller?

  layout :layout_by_resource

  protected
  def layout_by_resource
    return "layouts/session" if devise_controller?
    "layouts/application"
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:first_name, :surname, :email, :password, :password_confirmation, :remember_me) }
    devise_parameter_sanitizer.permit(:sign_in) { |u| u.permit(:first_name, :surname, :email, :password, :remember_me) }
    devise_parameter_sanitizer.permit(:account_update) {|u| u.permit(:first_name, :surname, :email, :password, :password_confirmation, :current_password)}
  end

  def store_current_location
    store_location_for(:user, request.url)
    store_location_for(:manager, request.url)
  end

end
