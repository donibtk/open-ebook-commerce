class EbooksController < ApplicationController

  def ebooks
    @ebook = Ebook.find(params[:id])
    raise ActionController::RoutingError.new('Not Found') if @ebook.available == false
  end

  def download
    ebook = current_user.ebooks.find_by_id(params[:id])
    raise ActionController::RoutingError.new('Not Found') if !ebook
    if params[:type] == "pdf"
      send_file(ebook.pdf.path, :filename => "#{ebook.title}.pdf")
    elsif params[:type] == "epub"
      send_file(ebook.epub.path, :filename => "#{ebook.title}.epub")
    end
  end

end
