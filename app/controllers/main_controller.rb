class MainController < ApplicationController


  def home
    @ebooks = Ebook.search(params[:s], params[:page], params[:c])
  end

  def contact
  end

  def about
  end

end
