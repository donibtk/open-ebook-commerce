class OrderController < ApplicationController
  include Concurrent::Async
  before_action :authenticate_user!, except: :notify

  def order_confirm
    session[:redirect_url] = nil
    if session[:cart].nil? || session[:cart].empty?
      redirect_to root_url
    else
      ebooks_in_cart
    end
  end

  def ebooks_in_cart
    @ebooks = []
    @cart_total = 0.0
    @removed = [];

    ebook_list = session[:cart];
    ebook_list.each do |e|
      ebook = Ebook.find(e)
      if current_user.has_ebook?(e)
        @removed << ebook
      else
        @ebooks << ebook
        @cart_total += ebook.price
      end
    end
    @removed.each {|r|
      session[:cart] = ebook_list.delete r.id }
  end

  def create_payment
    if !session[:redirect_url].nil?
      redirect_to session[:redirect_url]
      session[:redirect_url] = nil
      return
    end

    ebooks_in_cart

    order = current_user.orders.create(total: @cart_total, paid: false)
    payment = PagSeguro::PaymentRequest.new

    payment.reference = order.id
    payment.notification_url = order_notify_url #"https://5c478c65.ngrok.io/order/notify"
    payment.redirect_url = order_confirmation_url #"https://5c478c65.ngrok.io/order/confirmation"

    @ebooks.each do |e|
      order.ebook_orders.create(value: e.price, ebook: e)
      payment.items << {
        id: e.id,
        description: e.title,
        amount: e.price,
        weight: 0
      }
    end

    response = payment.register

    if response.errors.any?
      order.destroy
      raise response.errors.join("\n")
    else
      session[:redirect_url] = response.url
      redirect_to response.url
      session[:cart] = []
      return
    end
  end

  skip_before_action :verify_authenticity_token, only: :notify
  def notify
    async.process_payment params[:notificationCode]

    render body: nil, status: 200
  end

  def mail_last_order
    order = Order.last
    UserMailer.ebooks_download(order.user, order).deliver_now
    redirect_to root_url
  end

  def process_payment(token)
    transaction = PagSeguro::Transaction.find_by_notification_code(token)

    if transaction.errors.empty?
      order = Order.find(transaction.reference)
      order.update_attribute(:paid, true) if transaction.status.id == "3" # pago
      UserMailer.ebooks_download(order.user, order).deliver_now
    end
  end

end
