class CartController < ApplicationController

  def cart
    @ebooks_in_cart = []
    @cart_total = 0.0
    ebook_list = session[:cart];
    if ebook_list
      ebook_list.each do |e|
        ebook = Ebook.find(e)
        @ebooks_in_cart << ebook
        @cart_total += ebook.price
      end
    end
  end

  def add_to_cart
    id = params[:ebook_id]

    if user_signed_in?
      ebook = Ebook.find(id)
      if current_user.has_ebook?(id)
        flash.alert = "Você já possui o livro #{ebook.title}"
        redirect_to cart_url
        return
      end
    end

    session[:cart] = [] if session[:cart].nil?
    session[:cart] << id
    session[:cart].uniq!
    redirect_to cart_url
  end

  def remove_from_cart
    if not(session[:cart].nil? || session[:cart].empty?) && session[:cart].is_a?(Array)
      session[:cart].delete params[:ebook_id]
    end
    redirect_to cart_url
  end

end
