class UserController < ApplicationController
before_action :authenticate_user!

  def orders
    @orders = Order.where("user_id = ? AND created_at > ?", current_user.id, 30.days.ago)
  end
end
