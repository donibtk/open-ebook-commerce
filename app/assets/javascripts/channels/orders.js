//= require cable
//= require_self
//= require_tree .

App.orders = App.cable.subscriptions.create('OrdersChannel', {
  received: function(data) {
    return this.renderMessage(data);
  },

  renderMessage: function(data) {
    return $('#order-' + data.id).
            fadeOut(600, function() {
              $(this).html(data.content);
            }).
            fadeIn(600);
  }
});
