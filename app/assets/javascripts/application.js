// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require turbolinks
//= require ckeditor/init
//= require jquery.infinitescroll

function updateBodyPadding() {
    nav_height = $("div.container-fluid").first().height();
    $("body").first().css('padding-top', nav_height + 15);

    if($(window).width() <= 555) {
        $("div.ebook").css({"display" : "block"});
    } else {
        $("div.ebook").css({"display" : "inline-block"});
    }
}

$(function() {
    updateBodyPadding();
    $(window).on('resize', updateBodyPadding);
});
