class ApplicationMailer < ActionMailer::Base
  default from: 'dbaltokoski@alunos.utfpr.edu.br'
  layout 'mailer'
end
