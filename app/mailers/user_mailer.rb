class UserMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.ebooks_download.subject
  #
  def ebooks_download(order)
    @order = order
    mail to: order.user.email, subject: "Seus novos ebooks!"
  end
end
