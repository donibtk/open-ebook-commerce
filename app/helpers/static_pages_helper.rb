module StaticPagesHelper


  def available_ebooks
    Ebook.where(available: true)
  end

  def number_to_currency_br(number)
    number_to_currency(number, :unit => "R$ ", :separator => ",", :delimiter => ".")
  end

  def all_tags
    ActsAsTaggableOn::Tag.most_used(ActsAsTaggableOn::Tag.count)
  end

  def category
    c = params[:c]
    return c.capitalize if !c.nil?
    return "Todos"
  end

  def cart_not_empty?
    return false if session[:cart].nil? || session[:cart].empty?
    true
  end

  def category_links
    @ebook.genders.map { |e|
      "<a href='#{root_url}?c=#{e.name}'>#{e.name.capitalize}</a>"
    }.join(", ")
  end
end
