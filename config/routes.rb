Rails.application.routes.draw do
  root to: "main#home"
  get '/', to: 'main#home'

  get '/ebooks/:id', to: 'ebooks#ebooks'
  get '/download/:id', to: 'ebooks#download', as: 'download'

  get '/cart', to: 'cart#cart'
  post '/cart', to: 'cart#add_to_cart'
  delete '/cart', to: 'cart#remove_from_cart'

  get '/order/checkout', to: 'order#order_confirm'
  post '/order/notify', to: 'order#notify'
  get '/order/notify', to: 'order#mail_last_order'
  get '/order/create', to: 'order#create_payment'
  get '/order/confirmation', to: 'order#confirmation'

  get '/orders', to: 'user#orders'

  #get 'main/contact'
  #get '/about', to: 'main#about'

  devise_for :users

  devise_for :managers
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get '/managers', to: redirect('/admin')

  mount ActionCable.server => '/cable'

end
