set :stage, :staging

server '162.243.4.178', roles: %w(app web db), primary: true, user: 'deployer'
set :rails_env, "production"
