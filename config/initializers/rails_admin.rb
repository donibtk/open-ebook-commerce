RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
   warden.authenticate! scope: :manager
  end
  config.current_user_method(&:current_manager)

  config.included_models = [Manager, User, Ebook, Order]

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  config.show_gravatar = true

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      except ['Order']
    end
    export
    bulk_delete
    show
    edit
    delete
    show_in_app
    toggle

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end

  config.model 'Manager' do
    label "Gerente"
    label_plural "Gerentes"

    list do
      field :email
      field :updated_at do
        label "Ultima atualização"
      end
      field :approved, :toggle do
        label "Aprovado"
      end
    end

    edit do
      field :email
      field :password do
        label "Senha"
      end
      field :password_confirmation do
        label "Confirmação de Senha"
      end
      field :first_name do
        label "Nome"
      end
      field :surname do
        label "Sobrenome"
      end
      field :approved do
        label "Aprovado"
      end
    end

    show do
      field :email
      field :first_name do
        label "Nome"
      end
      field :surname do
        label "Sobrenome"
      end
      field :approved do
        label "Aprovado"
      end
      field :last_sign_in_at do
        label "Último login"
      end
      
    end


  end

  config.model 'Ebook' do
    label "e-books"

    list do
      field :id
      field :cover do
        label "Capa"
      end
      field :title do
        label "Título"
      end
      field :price do
        label "Preço"
        formatted_value do
          value.to_f.to_currency(Currency::BRL)
        end
      end
      field :available, :toggle do
        label "Disponivel"
      end
    end
    edit do
      field :title do
        label "Título"
        html_attributes do
          {:style => "width:90%"}
        end
      end
      field :author do
        label "Autor"
        html_attributes do
          {:style => "width:90%"}
        end
      end
      field :cover do
        label "Capa"
      end
      field :words do
        label "Palavras"
      end
      field :price do
        label "Preço"
      end
      field :summary, :ck_editor do
        label "Resumo"
      end
      field :year do
        label "Ano"
      end
      field :pdf do
        label "PDF"
      end
      field :epub do
        label "EPUB"
      end
      field :available do
        label "Disponivel"
      end
      field :gender_list do
        label "Gêneros"
        html_attributes do
          {:style => "width:90%"}
        end
      end
    end

    show do
      field :title do
        label "Título"
      end
      field :author do
        label "Autor"
      end
      field :cover do
        label "Capa"
      end
      field :words do
        label "Palavras"
      end
      field :price do
        label "Preço"
        formatted_value do
          value.to_f.to_currency(Currency::BRL)
        end
      end
      field :summary, :ck_editor do
        label "Resumo"
      end
      field :year do
        label "Ano"
      end
      field :pdf do
        label "PDF"
      end
      field :epub do
        label "EPUB"
      end
      field :available do
        label "Disponivel"
      end
    end
  end

  config.model 'Order' do
    label "Compras"

    list do
      field :id
      field :user do
        label "Usuário"
      end
      field :total do
        formatted_value do
          value.to_f.to_currency(Currency::BRL)
        end
      end
      field :paid, :toggle do
        label "Pago"
      end
      field :created_at do
        label "Data da compra"
      end
    end

    show do
      field :user do
        label "Usuário"
      end
      field :total do
        formatted_value do
          value.to_f.to_currency(Currency::BRL)
        end
      end
      field :paid do
        label "Pago"
        formatted_value do
          value ? "Sim" : "Não"
        end
      end
      field :created_at do
        label "Data da compra"
      end
      field :ebooks do
        label "e-books"
      end
    end

    edit do
      field :user do
        read_only true
        label "Usuário"
      end
      field :total do
        read_only true
        formatted_value do
          value.to_f.to_currency(Currency::BRL)
        end
      end
      field :paid do
        label "Pago"
        formatted_value do
          value ? "Sim" : "Não"
        end
      end
      field :created_at do
        read_only true
        label "Data da compra"
      end
      field :ebooks do
        read_only true
        label "e-books"
      end
    end

  end

  config.model 'User' do

    list do
      field :email
      field :name
      field :last_sign_in_at do
        label 'Último login'
      end
    end

    show do
      field :email
      field :name
      field :last_sign_in_at do
        label 'Último login'
      end
    end
    
    edit do
      field :email
      field :first_name do
        label 'Nome'
      end
      field :surname do
        label 'Sobrenome'
      end
      field :password do
        label "Senha"
      end
      field :password_confirmation do
        label "Confirmação de Senha"
      end
    end    
    

  end
  

end
