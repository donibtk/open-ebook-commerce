require 'test_helper'

class DownloadEbooksTest < ActionDispatch::IntegrationTest

  def test_user_can_download_ebook
    user = FactoryGirl.create(:user)
    order = user.orders.create(paid: true)
    ebook = FactoryGirl.create(:active_ebook1)
    order.ebooks << ebook

    visit orders_url
    fill_in 'Email', with: user.email
    fill_in 'Senha', with: 'abc123'
    click_button 'Entrar'

    page.must_have_content "pdf", count: 1
    page.must_have_content "epub", count: 1
    page.must_have_content ebook.title, count: 1

    click_link 'pdf'
    assert_match page.response_headers['Content-Type'], 'application/pdf'

    visit orders_url
    click_link 'epub'
    assert_match page.response_headers['Content-Type'], 'application/octet-stream'
  end

end
