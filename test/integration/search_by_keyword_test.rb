require 'test_helper'

class SearchByKeywordTest < ActionDispatch::IntegrationTest

  test "shold search ebook by keyword" do

    active_ebook1 = FactoryGirl.build(:active_ebook1)
    active_ebook1.save
    active_ebook2 = FactoryGirl.build(:active_ebook2)
    active_ebook2.save
    visit root_url
    not_active_ebook1 = FactoryGirl.build(:not_active_ebook1)
    not_active_ebook1.save

    visit root_url

    expect(find_field('Procurar').value)
    page.must_have_content "Pôr no carrinho", count: 2
    fill_in 'Procurar', with: "king"
    click_button ""
    page.must_have_content "Pôr no carrinho", count: 1
    page.must_have_content "One active ebook", count: 1

    fill_in 'Procurar', with: "herbert"
    click_button ""
    page.must_have_content "Pôr no carrinho", count: 1
    page.must_have_content "Two active ebook", count: 1

    fill_in 'Procurar', with: "notfound"
    click_button ""
    page.must_have_content "Pôr no carrinho", count: 0

    fill_in 'Procurar', with: "active"
    click_button ""
    page.must_have_content "Pôr no carrinho", count: 2
    page.must_have_content "One active ebook", count: 1
    page.must_have_content "Two active ebook", count: 1

    active_ebook1.destroy
    active_ebook2.destroy
    not_active_ebook1.destroy
  end
end
