require 'test_helper'

class CartPageTest < ActionDispatch::IntegrationTest

  test "shold add and remove ebook from cart" do

    active_ebook1 = FactoryGirl.build(:active_ebook1)
    active_ebook1.save
    visit root_url
    page.must_have_content "Pôr no carrinho", count: 1
    click_button "Pôr no carrinho"

    visit root_url
    page.must_have_content "Ir para Carrinho (1)", count: 1
    click_link "Ir para Carrinho (1)"
    page.must_have_content "Total:", count: 1

    visit root_url
    click_button "Pôr no carrinho" # adicionar apenas uma vez
    page.must_have_content "Remover", count: 1
    click_button "Remover"
    page.must_have_content "Carrinho vazio", count: 1

    visit root_url
    page.must_have_content "Ir para Carrinho (0)", count: 1
    active_ebook1.destroy
  end
end
