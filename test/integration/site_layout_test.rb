require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
   visit root_url
   page.must_have_content "OpenEbookCommerce", count: 1
   page.must_have_content "Categoria:", count: 1
   page.must_have_content "Todos", count: 3
   page.must_have_content "Entrar", count: 1
   page.must_have_content "Ir para Carrinho (0)", count: 1
   page.has_xpath?("//a",:text => "OpenEbookCommerce")
  end

  test "layout ebook" do
   active_ebook1 = FactoryGirl.build(:active_ebook1)
   active_ebook1.save
   visit "/ebooks/#{active_ebook1.id}"
   page.must_have_content "OpenEbookCommerce", count: 1
   page.must_have_content "Todos", count: 1
   page.must_have_content "Entrar", count: 1
   page.must_have_content "Ir para Carrinho (0)", count: 1
   page.must_have_content "Pôr no carrinho", count: 1
   page.must_have_content "R$  2,60", count: 1
   page.has_xpath?("//a",:text => "OpenEbookCommerce")
   page.has_selector?"div.ebook-body", count: 1
   page.has_selector?"img.ebook-cover", count: 1
   page.has_selector?"p.ebook-page-price", count: 1
   page.has_selector?"a.btn-default", count: 1
   page.must_have_content "Suspence", count: 2
   page.must_have_content "Terror", count: 2
   active_ebook1.destroy
  end


end
