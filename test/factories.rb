include ActionDispatch::TestProcess

class ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods
end

FactoryGirl.define do
  factory :ebook_order do
    value "9.99"
  end
  factory :order do
    paid false
    total "9.99"
  end
  factory :user do
    email "teste@teste.com"
    password "abc123"
    password_confirmation "abc123"
  end
  factory :active_ebook1, class: Ebook do
    title "One active ebook"
    author "Some author"
    words 600
    price 2.6
    gender_list {"terror,suspence"}
    summary "king "
    year 1900
    available true
    cover {fixture_file_upload(Rails.root.join('test', 'files', 'cover.jpg'), 'image/jpeg') }
    epub {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.epub'), 'application/epub+zip') }
    pdf {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.pdf'), 'application/pdf') }
  end
  factory :active_ebook2, class: Ebook do
    title "Two active ebook"
    author "herbert"
    words 600
    price 2.6
    gender_list {"terror,policial"}
    summary "Um livro de "
    year 1900
    available true
    cover {fixture_file_upload(Rails.root.join('test', 'files', 'cover.jpg'), 'image/jpeg') }
    epub {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.epub'), 'application/epub+zip') }
    pdf {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.pdf'), 'application/pdf') }
  end
  factory :not_active_ebook1, class: Ebook do
    title "Two active ebook"
    author "Some author"
    words 600
    price 2.6
    gender_list {"terror,policial"}
    summary "notfound"
    year 1900
    available false
    cover {fixture_file_upload(Rails.root.join('test', 'files', 'cover.jpg'), 'image/jpeg') }
    epub {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.epub'), 'application/epub+zip') }
    pdf {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.pdf'), 'application/pdf') }
  end
  factory :not_active_ebook2, class: Ebook do
    title "Two active ebook"
    author "Some author"
    words 600
    price 2.6
    gender_list {"romance,policial"}
    summary "Um livro de "
    year 1900
    available false
    cover {fixture_file_upload(Rails.root.join('test', 'files', 'cover.jpg'), 'image/jpeg') }
    epub {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.epub'), 'application/epub+zip') }
    pdf {fixture_file_upload(Rails.root.join('test', 'files', 'ebook.pdf'), 'application/pdf') }
  end
end
