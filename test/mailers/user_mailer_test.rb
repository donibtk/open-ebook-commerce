require "test_helper"

class UserMailerTest < ActionMailer::TestCase

  def test_ebooks_download
    user = FactoryGirl.create(:user)
    order = user.orders.create(paid: true)
    ebook = FactoryGirl.create(:active_ebook1)
    order.ebooks << ebook

    mail = UserMailer.ebooks_download order
    assert_equal "Seus novos ebooks!", mail.subject
    assert_equal ["teste@teste.com"], mail.to
    assert_equal ["dbaltokoski@alunos.utfpr.edu.br"], mail.from
    assert_match ebook.title, mail.body.encoded
    assert_match "PDF", mail.body.encoded
    assert_match "EPUB", mail.body.encoded
  end

end
