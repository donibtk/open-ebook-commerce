# Preview all emails at http://localhost:3000/rails/mailers/user_mailer
class UserMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/user_mailer/ebooks_download
  def ebooks_download
    UserMailer.ebooks_download(User.first, User.last.orders.last)
  end

end
