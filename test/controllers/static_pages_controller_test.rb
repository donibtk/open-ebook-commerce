require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get home" do
    visit root_url
    page.has_title? full_title("Todos")
  end

  #test "should get ebook" do
    #get "#{root_url}/ebooks/1"
    #assert_response :success
    #assert_select "title", full_title("FirstEbook")
  #end


  #test "should get about" do
  #  get static_pages_about_url
  #  assert_response :success
  #end

end
