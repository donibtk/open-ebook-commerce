require "test_helper"

class UserTest < ActiveSupport::TestCase
  def user
    @user ||= User.new
  end

  def valid_user
    User.new(email: 'example@example.com', password: 'abc123', password_confirmation: 'abc123')
  end

  def test_not_valid_user
    assert_not user.valid?
  end

  def test_valid_user
    u = user
    u.email = 'example@example.com'
    assert_not u.valid?
    u.password_confirmation = 'abc123'
    assert_not u.valid?
    u.password = 'abc123'
    assert u.valid?
  end

  def test_user_have_ebook
    u = valid_user
    u.save
    o = Order.new(paid: true, user_id: u.id)
    o.save
    e = FactoryGirl.build(:active_ebook1)
    e.save
    eo = EbookOrder.new(ebook_id: e.id, order: o)
    eo.save
    assert_equal u.ebooks.count, 1
    e = FactoryGirl.build(:active_ebook2)
    e.save
    eo = EbookOrder.new(ebook: e, order: o)
    eo.save
    assert_equal u.ebooks.count, 2
  end

  def test_user_doesnt_have_ebook
    u = valid_user
    u.save
    o = Order.new(paid: false, user_id: u.id)
    o.save
    e = FactoryGirl.build(:active_ebook1)
    e.save
    eo = EbookOrder.new(ebook_id: e.id, order: o)
    eo.save
    assert_equal u.ebooks.count, 0
    e = FactoryGirl.build(:active_ebook2)
    e.save
    eo = EbookOrder.new(ebook: e, order: o)
    eo.save
    assert_equal u.ebooks.count, 0
  end
end
