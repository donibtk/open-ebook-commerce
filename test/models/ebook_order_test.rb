require "test_helper"

class EbookOrderTest < ActiveSupport::TestCase
  def ebook_order
    @ebook_order ||= EbookOrder.new
  end

  def test_valid
    assert ebook_order.valid?
  end
end
