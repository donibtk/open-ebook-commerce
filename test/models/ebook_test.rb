require 'test_helper'

class EbookTest < ActiveSupport::TestCase

  def test_destroied_ebook_dont_let_files
    e = FactoryGirl.build(:active_ebook1)
    e.save
    pdf = e.pdf.path
    epub = e.epub.path
    cover = e.cover.path
    assert File.exist?(pdf)
    assert File.exist?(epub)
    assert File.exist?(cover)

    e.destroy

    assert_not File.exist?(pdf)
    assert_not File.exist?(epub)
    assert_not File.exist?(cover)

  end
end
